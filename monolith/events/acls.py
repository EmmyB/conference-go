
import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    headers={"Authorization": PEXELS_API_KEY}
    params={
        "per_page": 1,
        "query": city + "" + state,
    }
    url= "http://api.pexels.com/v1/search"

    response=requests.get(url, params=params, headers=headers)
    content= json.loads(response.content)

    return{"picture_url": content["photos"][0]["src"]["original"]}









    # GET_PHOTO GUIDE:
    # Create a dictionary for the headers to use in the request
    # Create the URL for the request with the city and state
    # Make the request
    # Parse the JSON response
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response


def get_weather_data(city, state):
    url=f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit={1}&appid={OPEN_WEATHER_API_KEY}"
    response=requests.get(url)
    content=json.loads(response.text)

    lat=content[0]["lat"]
    lon=content[0]["lon"]

    url=f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&units=standard&appid={OPEN_WEATHER_API_KEY}"
    response=requests.get(url)
    content=json.loads(response.text)

    temp=content["main"]["temp"]
    desc=content["weather"][0]["description"]

    return {
        "temp":temp,
        "desc":desc
    }






    # Create the URL for the geocoding API with the city and state
    # 👍
    # Make the request
    # 👍
    # Parse the JSON response
    # 👍
    # Get the latitude and longitude from the response
    # 👍


    # Create the URL for the current weather API with the latitude
    #   and longitude
    # 👍
    # Make the request
    # 👍
    # Parse the JSON response
    # 👍
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # 👍
    # Return the dictionary
    # 👍